/**
 * Name: Pimp-A-Fy - Module
 * Author: Tony Birleffi
 * Version: Alpha 1
 * 
 * Requires: GSAP - TweenMax.
 *
 * A basic javascript class module for doing animation faster. 
 * A quicker way to create global animations, that doesn't require a lot of extra scripting.
**/

!function(global, TweenMax) {
	'use strict';

	var previousPimpAFy = global.PimpAFy;

	function PimpAFy(options) {
		PimpAFy.widgetName = 'PimpAFy';
		PimpAFy.customMethodPrefix = 'c-';
		PimpAFy.isFunctionPrefix = 'f:';
		PimpAFy.isFunctionEventDispatchPrefix = 'fd:';
		PimpAFy.isTweenObjPrefix = '|';
		PimpAFy.isTimelinesPrefix = '~';
		PimpAFy.knownMethods = [];
		PimpAFy.gradientTweenList = [];
		PimpAFy.masterScript = '';
		PimpAFy.masterScriptType = 'normal';

		if(typeof options === "undefined") {
			PimpAFy.masterSpeed = .25;
			PimpAFy.masterEase = 'Quad.easeOut';
		} else {
			PimpAFy.masterSpeed = (typeof options.speed === "undefined") ? .25 : options.speed;
			PimpAFy.masterEase = (typeof options.ease === "undefined") ? 'Quad.easeOut' : options.ease;
			
			if(typeof options.customMethods !== 'undefined' && options.customMethods.length > 0) {
				for(var key in options.customMethods) {
					PimpAFy.knownMethods.push(options.customMethods[key]);
				}
			}

			if(typeof options.timeline !== 'undefined' && options.timeline.length > 0) {
				PimpAFy.masterScript = options.timeline;
			}

			if(typeof options.gradTimeline !== 'undefined' && options.gradTimeline.length > 0) {
				PimpAFy.masterScript = options.gradTimeline;
				PimpAFy.masterScriptType = 'grad';
			}
		}

		defineKnownMethods();
		if (PimpAFy.masterScript.length > 0) {
			runMasterTimeline();
		} else {
			initialize();
		}
	}

	function defineKnownMethods() {
		PimpAFy.knownMethods.push('p-tween');
		PimpAFy.knownMethods.push('p-timeline');
		PimpAFy.knownMethods.push('p-grad-timeline');
		PimpAFy.knownMethods.push('p-fade-in');
		PimpAFy.knownMethods.push('p-fade-out');
		PimpAFy.knownMethods.push('p-gradient');
	}

	function initialize() {
		var items = document.getElementsByTagName("*");
	    for (var i = 0; i < items.length; i++) 
	    {
	        var element = items[i];
	        PimpAFy.run(element);
	    }
	}

	function runMasterTimeline() {
		var fileType = (PimpAFy.masterScript.match(/[^\\\/]\.([^.\\\/]+)$/) || [null]).pop();
		if(fileType === 'json') {
			var c = new XMLHttpRequest();
			c.open('GET', PimpAFy.masterScript);
			c.onload = function() {
				var script = c.responseText;
				var json = JSON.parse(script || "null");
				if(json) {
					var data = json.tweens;
					for(var key in data) {
						var id = data[key].id;
						if(typeof id  !== 'undefined') {
							var element = document.getElementById(id);
							(PimpAFy.masterScriptType === 'normal') ? PimpAFy.pTween(element, '', data[key]) : PimpAFy.pGradient(element, '', data[key]);
						}
					}
				}
			}
			c.send();
		} else {
			throw new Error(PimpAFy.widgetName + ': Please provide a .json formatted file.');
		}
	}

	function camelCased(str) 
	{
		var camelCased = str.replace(/[-_ .]+(.)?/g, function (match, p) { if (p) return p.toUpperCase(); return ''; }).replace(/[^\w]/gi, '');
		return camelCased;
	}

	function parseMethodTagValues(element, methodTag) {
		var obj = element.getAttribute(methodTag);
		return parseEachTweenObj(obj);
	}

	function parseEachTweenObj(obj) {
		var fields = obj.split(', '); 
		var fieldObject = {};

		if( typeof fields === 'object' && fields.length > 0 ) {
			fields.forEach(function(field) {
				var c = field.split(PimpAFy.isTweenObjPrefix);
			    fieldObject[c[0]] = c[1];
			});
		}

		for (var key in fieldObject) {
			if (fieldObject.hasOwnProperty(key)) {
				if(typeof fieldObject[key] === 'undefined') {
					return null;
				}
			}
		}

		return fieldObject;
	}

	function parseMultipleTweensJSON(element, methodTag, json) {
		var timelines = json; 
		var tweens = [];

		if( typeof timelines === 'object' && timelines.length > 0 ) {
			for (var key in timelines) {
				var c = parseEachTweenObj(timelines[key]);
				tweens.push(c);
			}
		}

		for (var key in tweens) {
			if(!tweens[key]) return null;
		}

		return tweens;
	}

	function parseMultipleTweens(element, methodTag, script) {
		var obj = ( (typeof script !== 'undefined') ? script : element.getAttribute(methodTag) );
		var timelines = obj.split(' ' + PimpAFy.isTimelinesPrefix + ' '); 
		var tweens = [];

		if( typeof timelines === 'object' && timelines.length > 0 ) {
			for (var key in timelines) {
				var c = parseEachTweenObj(timelines[key]);
				tweens.push(c);
			}
		}

		for (var key in tweens) {
			if(!tweens[key]) return null;
		}

		return tweens;
	}

	function formatData(data) {
		for(var key in data) {
			if( data[key] === 'true' ) data[key] = true;
			if( data[key] === 'false' ) data[key] = false;
			if( String(data[key]).indexOf(PimpAFy.isFunctionPrefix) === 0 ) {
				var f = data[key];
				f = f.replace(PimpAFy.isFunctionPrefix, '');
				f = eval(f);
				data[key] = f;
			}
			if( String(data[key]).indexOf(PimpAFy.isFunctionEventDispatchPrefix) === 0 ) {
				var f = data[key];
				f = f.replace(PimpAFy.isFunctionEventDispatchPrefix, '');
				var nf = eval(function() {
					var cte = null;
					if(document.createEvent) {
						cte = document.createEvent('Event');
						cte.initEvent(f, true, true);
					} else {
						cte = new CustomEvent(f);
					}
					document.dispatchEvent(cte);
				});
				data[key] = nf;
			}
		}
		return data;
	}

	function stripBracketsToArray(obj) {
		var arr = [];
		obj = obj.replace('[', '');
		obj = obj.replace(']', '');
		arr = obj.split(',');
		return arr;
	}

	function convertArrayToObject(key, arr) {
		var obj = {};
		for (var i = 0; i < arr.length; i++) obj[key + i] = arr[i];
		return obj;
	}

	function stringifyLocColorObj(locations, colors)
	{
		var collect = [];
		if( Object.keys(locations).length == Object.keys(colors).length )
		{
			var cnt = 0;
			for (var key in colors)
			{
				var eachLoc = locations['loc' + cnt];
				var eachColor = colors['color' + cnt];
				if(typeof(eachLoc) !== 'undefined' && typeof(eachColor) !== 'undefined')
				{
					collect.push( String( eachColor + ' ' + eachLoc) );
				}
				cnt++;
			}

			var r = collect.toString();
			if(r.length > 0) return r;
		}
	}

	function stringifyLocColorObjCS(locations, colors)
	{
		var collect = [];
		if( Object.keys(locations).length == Object.keys(colors).length )
		{
			var cnt = 0;
			for (var key in colors) 
			{
				var eachLoc = locations['loc' + cnt];
				var eachColor = colors['color' + cnt];
				if(typeof(eachLoc) !== 'undefined' && typeof(eachColor) !== 'undefined')
				{
					collect.push( String( 'color-stop(' + eachLoc + ',' + eachColor + ')' ) );
				}
				cnt++;
			}

			var r = collect.toString();
			if(r.length > 0) return r;
		}
	}

	function runMSConvert(obj)
	{
		var msColorStart = obj.replace('rgb(', '');
		msColorStart = msColorStart.replace(')', '');
		msColorStart = msColorStart.split(',');
		msColorStart = rgb2hex(msColorStart[0], msColorStart[1], msColorStart[2]);
		return msColorStart;
	}

	function hexstr(number) 
	{
		var chars = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
		var low = number & 0xf;
		var high = (number >> 4) & 0xf;
		return "" + chars[high] + chars[low];
	}

	function rgb2hex(r, g, b) 
	{
		return "#" + hexstr(r) + hexstr(g) + hexstr(b);
	}

	function animateGradient(type, element, colorObj, locations)
	{
		var colorStr = stringifyLocColorObj(locations, colorObj);
		var colorStrCS = stringifyLocColorObjCS(locations, colorObj);
		var endLen = String(Object.keys(colorObj).length - 2);
		var msStartColor = runMSConvert( colorObj.color0 );
		var msEndColor = runMSConvert( colorObj['color' + endLen] );

		switch(type)
		{
			default:
			case 'horizontal':
					element.style.background = colorObj.color0; // Old browsers.
					element.style.background = 'url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzg3ZTBmZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iIzUzY2JmMSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwNWFiZTAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)'; // IE9
					element.style.background = '-moz-linear-gradient(left, ' + colorStr + ')'; // FF3.6+
					element.style.background = '-webkit-gradient(linear, left top, right top, ' + colorStrCS + ')'; // Chrome,Safari4+
					element.style.background = '-webkit-linear-gradient(left, ' + colorStr + ')'; // Chrome10+, Safari5.1+
					element.style.background = '-o-linear-gradient(left, ' + colorStr + ')'; // Opera 11.10+
					element.style.background = '-ms-linear-gradient(left, ' + colorStr + ')'; // IE10+
					element.style.background = 'linear-gradient(to right, ' + colorStr + ')'; // W3C
					element.style.filter = 'progid:DXImageTransform.Microsoft.gradient( startColorstr="' + msStartColor + '", endColorstr="' + msEndColor + '",GradientType=1'; // IE6-9
				break;

			case 'vertical':
					element.style.background = colorObj.color0; // Old browsers.
					element.style.background = 'url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzg3ZTBmZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iIzUzY2JmMSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwNWFiZTAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)'; // IE9
					element.style.background = '-moz-linear-gradient(top, ' + colorStr + ')'; // FF3.6+
					element.style.background = '-webkit-gradient(linear, left top, left bottom, ' + colorStrCS + ')'; // Chrome,Safari4+
					element.style.background = '-webkit-linear-gradient(top, ' + colorStr + ')' // Chrome10+, Safari5.1+
					element.style.background = '-o-linear-gradient(top, ' + colorStr + ')'; // Opera 11.10+
					element.style.background = '-ms-linear-gradient(top, ' + colorStr + ')'; // IE10+
					element.style.background = 'linear-gradient(to bottom, ' + colorStr + ')'; // W3C
					element.style.filter = 'progid:DXImageTransform.Microsoft.gradient( startColorstr="' + msStartColor + '", endColorstr="' + msEndColor + '",GradientType=0'; // IE6-9
				break;

			case 'diagonal-down-right':
					element.style.background = colorObj.color0; // Old browsers.
					element.style.background = 'url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzg3ZTBmZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iIzUzY2JmMSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwNWFiZTAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)'; // IE9
					element.style.background = '-moz-linear-gradient(-45deg, ' + colorStr + ')'; // FF3.6+
					element.style.background = '-webkit-gradient(linear, left top, right bottom, ' + colorStrCS + ')'; // Chrome,Safari4+
					element.style.background = '-webkit-linear-gradient(-45deg, ' + colorStr + ')'; // Chrome10+, Safari5.1+
					element.style.background = '-o-linear-gradient(-45deg, ' + colorStr + ')'; // Opera 11.10+
					element.style.background = '-ms-linear-gradient(-45deg, ' + colorStr + ')'; // IE10+
					element.style.background = 'linear-gradient(135deg, ' + colorStr + ')'; // W3C
					element.style.filter = 'progid:DXImageTransform.Microsoft.gradient( startColorstr="' + msStartColor + '", endColorstr="' + msEndColor + '",GradientType=1'; // IE6-9
				break;

			case 'diagonal-up-right':
					element.style.background = colorObj.color0; // Old browsers.
					element.style.background = 'url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzg3ZTBmZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iIzUzY2JmMSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwNWFiZTAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)'; // IE9
					element.style.background = '-moz-linear-gradient(45deg, ' + colorStr + ')'; // FF3.6+
					element.style.background = '-webkit-gradient(linear, left bottom, right top, ' + colorStrCS + ')'; // Chrome,Safari4+
					element.style.background = '-webkit-linear-gradient(45deg, ' + colorStr + ')'; // Chrome10+, Safari5.1+
					element.style.background = '-o-linear-gradient(45deg, ' + colorStr + ')'; // Opera 11.10+
					element.style.background = '-ms-linear-gradient(45deg, ' + colorStr + ')'; // IE10+
					element.style.background = 'linear-gradient(45deg, ' + colorStr + ')'; // W3C
					element.style.filter = 'progid:DXImageTransform.Microsoft.gradient( startColorstr="' + msStartColor + '", endColorstr="' + msEndColor + '",GradientType=1'; // IE6-9
				break;

			case 'radial':
					element.style.background = colorObj.color0; // Old browsers.
					element.style.background = 'url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzg3ZTBmZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjQwJSIgc3RvcC1jb2xvcj0iIzUzY2JmMSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwNWFiZTAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)'; // IE9
					element.style.background = '-moz-radial-gradient(center, ellipse cover, ' + colorStr + ')'; // FF3.6+
					element.style.background = '-webkit-gradient(radial, center center, 0px, center center, 100%, ' + colorStrCS + ')'; // Chrome,Safari4+
					element.style.background = '-webkit-radial-gradient(center, ellipse cover, ' + colorStr + ')'; // Chrome10+, Safari5.1+
					element.style.background = '-o-radial-gradient(center, ellipse cover, ' + colorStr + ')'; // Opera 11.10+
					element.style.background = '-ms-radial-gradient(center, ellipse cover, ' + colorStr + ')'; // IE10+
					element.style.background = 'radial-gradient(ellipse at center, ' + colorStr + ')'; // W3C
					element.style.filter = 'progid:DXImageTransform.Microsoft.gradient( startColorstr="' + msStartColor + '", endColorstr="' + msEndColor + '",GradientType=1'; // IE6-9
				break;
		}
	}

	function killAllGradientTweens()
	{
		for(var key in PimpAFy.gradientTweenList)
		{
			var tween = gradientTweenList[key];
			tween.kill();
		}
	}

	function tweenNormal(element, speed, data) {
		TweenMax.to(element, speed, data);
	}

	function tweenGradient(element, speed, data, colorObj) {
		var tween = TweenMax.to
		(
			colorObj, 
			speed, 
			{
				colorProps: data.colorsTo,
				ease: ( (typeof data.ease !== 'undefined') ? data.ease : PimpAFy.masterEase ),
				delay: ( (typeof data.delay !== 'undefined') ? data.delay : 0 ),
				onUpdate: function() 
				{
					animateGradient(data.gradientType, element, colorObj, data.locations);
					if(typeof data.onUpdate !== 'undefined') data.onUpdate();
				},
				onComplete: ( (typeof data.onComplete !== 'undefined') ? data.onComplete : null ),
				onStart: ( (typeof data.onStart !== 'undefined') ? data.onStart : null ),
			}
		);
		PimpAFy.gradientTweenList.push(tween);
	}

	function loadDataFromFile(filePath, element, methodTag) {
		var fileType = (filePath.match(/[^\\\/]\.([^.\\\/]+)$/) || [null]).pop();
		var c = new XMLHttpRequest();
		c.open('GET', filePath);
		c.onload = function() {
			var script = c.responseText;
			if(fileType === 'json') {
				var json = JSON.parse(script || "null");
				if(json) {
					var data = json.tweens;
				}
			} else {
				var data = parseMultipleTweens(element, methodTag, script);
			}

			if(data) {
				( methodTag == 'p-grad-timeline') ? runGradTimelineTweens(data, element, methodTag) : runTimelineTweens(data, element, methodTag);
			}
		}
		c.send();
	}

	function runGradTimelineTweens(data, element, methodTag) {
		if(data) {
			for(var key in data) {
				PimpAFy.pGradient(element, methodTag, data[key]);
			}
		} else {
			throw new Error(PimpAFy.widgetName + ': Please provide at least one tween object or a script file.');
		}
	}

	function runTimelineTweens(data, element, methodTag) {
		if(data) {
			for(var key in data) {
				PimpAFy.pTween(element, methodTag, data[key]);
			}
		} else {
			throw new Error(PimpAFy.widgetName + ': Please provide at least one tween object or a script file.');
		}
	}

	// ------------------------------------------------------------------------------------>
	// Supported animation methods.
	// ------------------------------------------------------------------------------------>

	PimpAFy.pGradient = function(element, methodTag, passData) {
		var data = ( (passData) ? passData : parseMethodTagValues(element, methodTag) );
		if(!data) data = {};

		var uiEvent = '';
		var colorsFrom = [];
		var colorsTo = [];
		var locations = [];
		var speed = ( (typeof data.speed !== 'undefined') ? data.speed : PimpAFy.masterSpeed );

		// gradientType.
		if(typeof data.gradientType === 'undefined') {
			throw new Error(PimpAFy.widgetName + ': pGradient requires the "gradientType" object.');
		}

		// colorsFrom.
		if(typeof data.colorsFrom !== 'undefined') {
			colorsFrom = stripBracketsToArray(data.colorsFrom);
			if(colorsFrom.length > 0) data.colorsFrom = colorsFrom;
		} else throw new Error(PimpAFy.widgetName + ': pGradient requires the "colorsFrom" object.');

		// colorsTo.
		if(typeof data.colorsTo !== 'undefined') {
			colorsTo = stripBracketsToArray(data.colorsTo);
			if(colorsTo.length > 0) data.colorsTo = colorsTo;
		} else throw new Error(PimpAFy.widgetName + ': pGradient requires the "colorsTo" object.');

		// locations.
		if(typeof data.locations !== 'undefined') {
			locations = stripBracketsToArray(data.locations);
			if(locations.length > 0) data.locations = locations;
		} else throw new Error(PimpAFy.widgetName + ': pGradient requires the "locations" object.');

		data.locations 	= convertArrayToObject('loc', data.locations);
		data.colorsFrom = convertArrayToObject('color', data.colorsFrom);
		data.colorsTo 	= convertArrayToObject('color', data.colorsTo);
		data.locations['_gsTweenID'] = '';

		if(typeof data.uiEvent !== 'undefined') {
			uiEvent = data.uiEvent;
			delete data['uiEvent'];
		}

		if(typeof data.id !== 'undefined') {
			delete data['id'];
		}

		var colorObj = data.colorsFrom;
		if(PimpAFy.gradientTweenList.length > 0) killAllGradientTweens();

		if(uiEvent.length == 0) {
			tweenGradient(element, speed, data, colorObj);
		} else {
			if( String(uiEvent).indexOf(PimpAFy.isFunctionPrefix) === 0 ) {
				uiEvent = uiEvent.replace(PimpAFy.isFunctionPrefix, '');
				document.addEventListener(uiEvent, function(e) {
					tweenGradient(element, speed, data, colorObj);
				});
			} else {
				element.addEventListener(uiEvent, function(e) {
					tweenGradient(element, speed, data, colorObj);
				});
			}
		}
	};

	PimpAFy.pFadeIn = function(element, methodTag) {
		var data = parseMethodTagValues(element, methodTag);
		if(!data) data = {};
		data.autoAlpha = 1;
		data.display = 'inherit';
		PimpAFy.pTween(element, methodTag, data);
	};

	PimpAFy.pFadeOut = function(element, methodTag) {
		var data = parseMethodTagValues(element, methodTag);
		if(!data) data = {};
		data.autoAlpha = 0;
		PimpAFy.pTween(element, methodTag, data);
	};

	PimpAFy.pTimeline = function(element, methodTag) {
		var data = parseMethodTagValues(element, methodTag);
		if(!data) data = {};

		if(typeof data.script !== 'undefined') {
			loadDataFromFile(data.script, element, methodTag);
		} else {
			data = parseMultipleTweens(element, methodTag);
			runTimelineTweens(data, element, methodTag);
		}
	};

	PimpAFy.pGradTimeline = function(element, methodTag) {
		var data = parseMethodTagValues(element, methodTag);
		if(!data) data = {};

		if(typeof data.script !== 'undefined') {
			loadDataFromFile(data.script, element, methodTag);
		} else {
			data = parseMultipleTweens(element, methodTag);
			runGradTimelineTweens(data, element, methodTag);
		}
	};

	PimpAFy.pTween = function(element, methodTag, passData) {
		try {
			if(element) {
				var data = {};
				var speed = PimpAFy.masterSpeed;
				var uiEvent = '';
				data = (passData) ? passData : parseMethodTagValues(element, methodTag);
				
				if(data) {
					if(typeof data.speed !== 'undefined') {
						speed = Number(data.speed);
						delete data['speed'];
					}

					if(typeof data.uiEvent !== 'undefined') {
						uiEvent = data.uiEvent;
						delete data['uiEvent'];
					}

					if(typeof data.id !== 'undefined') {
						delete data['id'];
					}

					data = formatData(data);
				} else {
					data = {};
				}

				if(uiEvent.length == 0) {
					tweenNormal(element, speed, data);
				} else {
					if( String(uiEvent).indexOf(PimpAFy.isFunctionPrefix) === 0 ) {
						uiEvent = uiEvent.replace(PimpAFy.isFunctionPrefix, '');
						document.addEventListener(uiEvent, function(e) {
							tweenNormal(element, speed, data);
						});
					} else {
						element.addEventListener(uiEvent, function(e) {
							tweenNormal(element, speed, data);
						});
					}
				}
			}
		} 
		catch(err) {
			throw new Error(PimpAFy.widgetName + ': Please provide an element to animate');
		}
	};

	PimpAFy.run = function(element) {
		for (var i = 0; i < PimpAFy.knownMethods.length; i++)
		{
			var method = PimpAFy.knownMethods[i];
			if( element.hasAttribute(method) )
			{
				var methodTag = method;
				method = ( methodTag.indexOf(PimpAFy.customMethodPrefix) === 0 ) ? camelCased(method) : ( PimpAFy.widgetName + '.' + camelCased(method) );
				var f = eval(method)(element, methodTag);
				try { f(element); } catch(err) {}
			}
		}
	};

	PimpAFy.noConflict = function noConflict() {
		global.PimpAFy = previousPimpAFy;
		return PimpAFy;
	};

	global.PimpAFy = PimpAFy;

}(this, TweenMax);


