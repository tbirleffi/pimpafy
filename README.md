# Pimp-A-Fy - JavaScript Module

---

A quicker way to create global animations, that doesn't require a lot of extra scripting.
Animate right in the HTML markup, how cool is that!

### Dependencies:
1. GreenSock - JS
2. jQuery - Just for the examples.

### Support:
It's been tested in most modern browsers, and it will work in IE9+.
```
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=10; IE=EDGE">
```

## Methods:
You can pass in any TweenMax friendly properties to animate on. To find out more:
http://greensock.com

### PimpAFy JavaScript Includes:
```
<!-- GSAP libraries. -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.0/TweenMax.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.0/plugins/ColorPropsPlugin.min.js"></script>

<!-- Local scripts. -->
<script src="../src/pimpafy.js"></script>
```

### PimpAFy CSS Includes:
```
<link rel="stylesheet" media='all' type="text/css" href="../src/pimpafy.css">
<style>
	.p-hide { display: none; opacity: 0; }
	.p-show { display: inherit; opacity: 1; }
	.p-hide-vis { opacity: 0; }
	.p-show-vis { opacity: 1; }
</style>
```

### PimpAFy.pTween:
```
<div id="Box" class="p-hide" p-tween="speed|2, ease|Quad.easeIn, autoAlpha|1, display|inherit"></div>
<div p-tween="<!-- Pass in any GSAP properties, with a pipe. -->"></div>
<script>
	$('#Box').attr('p-tween', '');
	PimpAFy.run( $('#Box')[0] );
</script>
```

### PimpAFy.pTimeline:
Note: For more fancy timeline tweening, I'd look at this:
http://greensock.com/docs/#/HTML5/Animation/TimelineLite

```
<!-- This element will first fade in, then change it's height to 500px, then 300px. -->
<!-- Then change it's width to 300px, and call 'onBox1Complete', after the timeline is complete. -->
<!-- The prefix to define multiple tweens is '~'. -->
<div class="box blue p-hide-vis" p-fade-in="speed|1" p-tween="height|500px, delay|3" p-timeline="height|300px, delay|6 ~ width|300px, delay|8, onComplete|f:onBox1Complete"></div>

<!-- With the prefix 'fd:', you can just dispatch another Javascript global event directly, instead of having it call to another JavaScript function somewhere. -->
<div class="box blue p-hide-vis" p-fade-in="speed|1" p-tween="height|500px, delay|3" p-timeline="height|300px, delay|6 ~ width|300px, delay|8, onComplete|fd:customTriggerEvent"></div>

<!-- You can also load in a PimpAFy script file, or any text file with the animation properties in it. -->
<div class="box blue p-hide-vis" p-fade-in="speed|1" p-timeline="script|assets/scripts/script2"></div>

<!-- The JSON format is also supported. -->
<div class="box blue p-hide-vis" p-fade-in="speed|1" p-timeline="script|assets/scripts/script3.json"></div>
```

### PimpAFy.pGradTimeline:
```
<!-- This element will animate a radial gradient on both mouseover and mouseout. -->
<div class="box blue-radial-grad" p-grad-timeline="gradientType|radial, colorsFrom|[#f0f9ff,#cbebff,#a1dbff], colorsTo|[#ffffff,#cffcc9,#9dd387], locations|[0%,45%,100%], speed|1, uiEvent|mouseover ~ gradientType|radial, colorsFrom|[#ffffff,#cffcc9,#9dd387], colorsTo|[#f0f9ff,#cbebff,#a1dbff], locations|[0%,45%,100%], speed|1, uiEvent|mouseout"></div>

<!-- You can also load in a PimpAFy script file, or any text file with the animation properties in it. -->
<div class="box blue-radial-grad" p-grad-timeline="script|assets/scripts/script1.pimpafy"></div>

<!-- The JSON format is also supported. -->
<div class="box blue-radial-grad" p-grad-timeline="script|assets/scripts/script4.json"></div>
```

### PimpAFy.pFadeIn:
```
<div id="Box" class="p-hide-vis" p-fade-in=""></div>
<script>
	$('#Box').attr('p-fade-in', '');
	PimpAFy.run( $('#Box')[0] );
</script>
```

### PimpAFy.pFadeOut:
```
<div id="Box" class="p-show" p-fade-out=""></div>
<div class="p-show" p-fade-out="display|none"></div><!-- To remove the element after, the fade out is done. -->
<script>
	$('#Box').attr('p-fade-out', '');
	PimpAFy.run( $('#Box')[0] );
</script>
```

### PimpAFy.pGradient:
```
<!-- This element will animate a radial gradient on the click event. -->
<!-- gradientType, colorsFrom, colorsTo & locations are all required to make it work. -->
<div class="box blue-radial-grad" p-gradient="gradientType|radial, colorsFrom|[#f0f9ff,#cbebff,#a1dbff], colorsTo|[#ffffff,#cffcc9,#9dd387], locations|[0%,45%,100%], speed|2, uiEvent|click"></div>
```

### Create Custom Animation Functions ( c-{function-name} ): 
```
<!-- It, must have a prefix of 'c-', in front of it. -->
<div class="p-hide" c-custom-method=""></div>
<script>
	PimpAFy({customMethods:['c-custom-method']});
	function cCustomMethod(element, methodTag) {
		TweenMax.to(element, 3, {autoAlpha: 1, display:'inherit'});
	}
</script>
```

### Define GSAP Functions ( {gsap-function}|f:{function-name} ):
```
<!-- 'f:', tells PimpAFy it's a function. -->
<div class="box pink-grad p-hide" p-tween="autoAlpha|1, display|inherit, onUpdate|f:onBoxUpdate"></div>

<!-- With the prefix 'fd:', you can just dispatch another Javascript global event directly, instead of having it call to another JavaScript function somewhere. -->
<div class="box pink-grad p-hide" p-tween="autoAlpha|1, display|inherit, onComplete|fd:customTriggerEvent"></div>

<script>
	function onBoxUpdate() {
		console.log('onBoxUpdate called.');
	}
</script>
```

### Setup UI Events (ex: click, mouseover, mouseout) ( uiEvent|{function-name} ):
```
<!-- This will fade in the element, on click. -->
<div class="box orange-grad p-hide" p-fade-in="uiEvent|click"></div>
```

### Setup Custom UI Events ( uiEvent|f:{custom-function-name} ):
```
<!-- 'f:', tells PimpAFy it's a function. In this example, the element won't fade in, untill the Javascript event: customTriggerEvent, has been dispatched. -->
<div class="box orange-grad p-hide" p-fade-in="uiEvent|f:customTriggerEvent">customTriggerEvent</div>
<script>
	var customTriggerEvent = new CustomEvent('customTriggerEvent');
	document.dispatchEvent(customTriggerEvent);
</script>
```

### Tween Between CSS Classes ( className|{css-class} ):
```
<!-- In this example, the element will animate from CSS classes 'box pink-grad' to CSS classes 'box orange'. -->
<div class="box pink-grad" p-tween="className|box orange, delay|2"></div>
<script>
	$('#Box').attr('p-tween', 'className|box orange, delay|2');
	PimpAFy.run( $('#Box')[0] );
</script>
```

### Setup Multiple Methods:
```
<!-- This element will first fade in, then fade out once the JavaScript event 'customTriggerEvent' has been dispatched. -->
<div class="box blue p-hide-vis" p-fade-in="speed|1" p-fade-out="speed|1, uiEvent|f:customTriggerEvent"></div>
```

### Master Timeline Script:
Don't want to mess with having your animations inline, use a master script JSON file instead. 
```
{
    "tweens": [
        {
            "id":"Box1",
            "width": "50px",
            "onComplete": "fd:showBox2",
            "delay":"1"
        },
        {
            "id":"Box2",
            "opacity": "1",
            "uiEvent":"f:showBox2"
        }
    ]
}

<div id="Box1" class="box blue"></div>
<div id="Box2" class="box orange p-hide-vis"></div>
<script>
	PimpAFy({timeline:'assets/scripts/timeline-example.json'});
</script>
```

### Master Gradient Timeline Script:
This is a seperate handler for gradient supported animations, but basically it still works the same. 
It just calls PimpAFy.pGradient, instead of PimpAFy.pTween. 
```
{
    "tweens": [
        {
            "id":"Box1",
            "gradientType": "vertical",
            "colorsFrom": "[#f0f9ff,#cbebff,#a1dbff]",
            "colorsTo": "[#ffffff,#cffcc9,#9dd387]",
            "locations": "[0%,45%,100%]",
            "speed":"1",
            "uiEvent":"mouseover"
        },
        {
            "id":"Box1",
            "gradientType": "vertical",
            "colorsFrom": "[#ffffff,#cffcc9,#9dd387]",
            "colorsTo": "[#f0f9ff,#cbebff,#a1dbff]",
            "locations": "[0%,45%,100%]",
            "speed":"1",
            "uiEvent":"mouseout"
        }
    ]
}

<div id="Box1" class="box blue"></div>
<script>
	PimpAFy({gradTimeline:'assets/scripts/grad-timeline-example.json'});
</script>
```

